require "spec_helper"
require "./lib/bibliografia/bibliografia.rb"
require "./lib/bibliografia/linkedlist.rb"
=begin
describe Bibliografia do
	before :each do
    	@libro = Bibliografia.new(["Dave Thomas"],"Programming Ruby 1.9 & 2.0: The Programatic Programers Guide","4 edition","Progmatic Bookshelf","July 7, 2013",["ISBN-13: 978-1937785499","ISBN-10: 1937785491"])
		@libro.m_serie = "Informática"
  	end

	it "Comprobar que hay por lo menos un autor" do
		@libro.m_autores.length.should_not be 0
	end

	it "Debe Existir un Título." do
		expect(@libro.m_titulo).to eq("Programming Ruby 1.9 & 2.0: The Programatic Programers Guide")
	end

	it "Debe existir o no una serie" do
		expect(@libro.m_serie).to eq("Informática")
	end

	it "Debe existir una editorial." do
		expect(@libro.m_editorial).to eq("Progmatic Bookshelf")
	end

	it "Debe existir un número de edición." do
		@libro.m_num_edicion != ""
	end

	it "Debe existir una fecha de publicación." do
		@libro.m_fechaPublicacion != ""
	end

	it " Debe existir uno o más números ISBN." do
	@libro.m_isbn.length.should_not be 0
	end

	it "Existe un método para obtener el listado de autores." do
		expect(@libro.m_autores).to eq(["Dave Thomas"])
	end

	it "Existe un método para obtener el título." do
		expect(@libro.m_titulo).to eq("Programming Ruby 1.9 & 2.0: The Programatic Programers Guide")
	end

	it "Existe un método para obtener la editorial." do
		expect(@libro.m_editorial).to eq("Progmatic Bookshelf")
	end

	it "Existe un método para obtener el número de edición" do
		expect(@libro.m_num_edicion).to eq("4 edition")
	end

	it "Existe un método para obtener la fecha de publicación." do
		expect(@libro.m_fechaPublicacion).to eq("July 7, 2013")
	end

	it "Existe un método para obtener el listado de ISBN" do
		expect(@libro.m_isbn).to eq(["ISBN-13: 978-1937785499","ISBN-10: 1937785491"])
	end

	it " Existe un método para obtener la referencia formateada." do
		expect(@libro.to_s).to eq("[\"Dave Thomas\"] Programming Ruby 1.9 & 2.0: The Programatic Programers Guide 4 edition Informática Progmatic Bookshelf July 7, 2013 [\"ISBN-13: 978-1937785499\", \"ISBN-10: 1937785491\"]")

	end
end
=end
describe Node do
    before:all do
        @libro = Bibliografia.new(["Dave Thomas"],"Programming Ruby 1.9 & 2.0: The Programatic Programers Guide","4 edition","Progmatic Bookshelf","July 7, 2013",["888af","añfa882"])
        @nodo = Node.new(@libro, nil)
    end
	it "#Debe existir un Nodo de la lista con sus datos y su siguiente" do
	     #expect(@nodo).instance_of?(Linkedlist)
	     expect(@nodo.value).not_to eq(nil)
	     expect(@nodo.next).to eq(nil)
	     
	end
end
describe Linkedlist do
	before:each do
	    @libro = Bibliografia.new(["Dave Thomas"],"Programming Ruby 1.9 & 2.0: The Programatic Programers Guide","4 edition","Progmatic Bookshelf","July 7, 2013",["888af","añfa882"])
		@nodo1= Node.new(@libro, nil)
		@lista = Linkedlist.new()
		@lista.insertarElemento([@nodo1])
	end
	it "#Existe una lista vacia" do
		expect(Linkedlist.new().empty?).to eq(true)
	end
	it "#Se extrae el primer elemento de la lista" do
	    nodoprueba= @nodo1
		@lista.extraerPrimerElemento
	end
	it "#Se puede insertar un elemento" do
		libro = Bibliografia.new(["Juan"],"Aprender ruby es divertido","1 edition","ULL","July 15, 2015",["ISBN-00: 288-1938885456"])

		nodo=Node.new(libro, nil)
		#expect(@lista.insertarElemento([nodo])).to eq([nodo])
		@lista.insertarElemento([nodo])
	end
	it "#Se pueden insertar varios elementos" do
		libro1 = Bibliografia.new(["Juan"],"Aprender c++ es divertido","1 edition","ULL","July 15, 2015",["ISBN-00: 288-1938885456"])
		libro2 = Bibliografia.new(["Juan"],"Aprender java es divertido","1 edition","ULL","July 15, 2015",["ISBN-00: 288-1938885456"])
	#	nodo2= Node.new(libro1, nil)
	#	nodo3= Node.new(libro2, nil)

	#	@lista.insertarElemento([nodo2,nodo3])
		@lista.insertar([libro1,libro2])
	end
	it "#Debe existir una Lista con su cabeza"do
	    expect(@lista.m_nodoInicio).to eq(@nodo1)
	end

=begin
libro1 = Bibliografia.new(["Dave Thomas","Andy Hunt", "Chad Fowler"], "Programming Ruby 1.9 & 2.0: The Pragmatic Programmer'Guide", "4 edition" ,"Pragmatic Bookshelf","July 7, 2013", ["ISBN-10: 1937785491","ISBN-10: 1937785491"])
libro1.m_serie ="The Facets of Ruby"
libro2 = Bibliografia.new(["Scott Chacon"],"Pro Git 2009th Edition.","2009 edition","Apress","August 27, 2009",["ISBN-13: 978-1430218333","ISBN-10: 1430218339"])
libro3= Bibliografia.new(["David Flanagan","Yukihiro Matsumoto"],"The Ruby Programming Lenguage","1 edition","O'Reilly Media", "February 4,2008",["ISBN-10: 0596516177","ISBN-13: 978-0596516178"])
libro4= Bibliografia.new(["David Chelimsky","Dave Astels","Bryan Helmekarmp", "Dan North","Zach Dennis", "Aslak Hellesoy"],"The RSpec Book: Behaviour Driven Development with RSpec, Cucumber, and Friends (The Facets of Ruby).","1 edition","Progmatic Bookshelf", "December 25, 2010",["ISBN-10: 1934356379","ISBN-13: 978-1934356371"])
libro5 = libro2 = Bibliografia.new(["Richard E. Silverman"],"Git Pocket Guide","1 edition","O'Reilly Media","August 13, 2013",["ISBN-10: 1449325866","ISBN-13: 978-1449325862"])
nodo= Node.new(libro1, nil)
nodo2= Node.new(libro2, nil)
nodo3= Node.new(libro3, nil)
nodo4= Node.new(libro4,nil)
nodo5=Node.new(libro5,nil)
lista = Linkedlist.new()
lista.size
lista.insertarElemento(nodo,nodo2,nodo3,nodo4,nodo5)
=end
end
