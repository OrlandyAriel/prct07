Node = Struct.new(:value, :next)

class Linkedlist
  #Se le llama m_nodoInicio a lo que la práctica llama "cabeza"
  attr_accessor :m_nodoInicio, :m_tamanio

  def initialize()
    @m_nodoInicio = Node.new(nil,nil)
    @m_tamanio = 0
  end
  
  #Función que comprueba que está vacio
  def empty?
    if @m_nodoInicio.value == nil
      true
    else
      false
    end
  end
  
  #Función para mostrar el tamaño
  def size
    p @m_tamanio
  end
  
  #Extrae el primer elemento de la lista
  def extraerPrimerElemento
    @m_tamanio = @m_tamanio-1
    @t_aux = @m_nodoInicio
    @m_nodoInicio = @m_nodoInicio.next
    @t_aux
  end
  
  #inserta uno o varios elementos a la lista, por el final
  #este metodo inserta nodos directamente.
  def insertarElemento(a_nodo)
    a_nodo.each do |nodo|
      if @m_nodoInicio.value == nil
        @m_nodoInicio = nodo
        @m_nodoInicio.next = nil
        @m_tamanio = 1
      else
        a_aux2 = @m_nodoInicio
        while a_aux2.next != nil do
          a_aux2 = a_aux2.next
        end
        a_aux2.next = nodo
        @m_tamanio = @m_tamanio+1
      end
    end
  end
  #inserta uno o varios elementos a la lista, por el final
  ## este metodo es diferente al otro, este inserta cualquier objeto y lo hace creando internamente el nodo
  def insertar(a_nodo)
    a_nodo.each do |nodo|
      nodoaux= Node.new(nodo,nil)
      if @m_nodoInicio.value == nil
        @m_nodoInicio = nodoaux
        @m_tamanio = 1
      else
        a_aux2 = @m_nodoInicio
        while a_aux2.next != nil do
          a_aux2 = a_aux2.next
        end
        a_aux2.next = nodoaux
        @m_tamanio = @m_tamanio+1
      end
    end
  end
end