
class Bibliografia
    attr_reader :m_autores , :m_titulo , :m_serie , :m_editorial , :m_num_edicion , :m_fechaPublicacion , :m_isbn 
    attr_writer :m_serie
    #Constructor con todos los parámetros
    def initialize(a_autores,a_titulo,a_num_edicion,a_editorial,a_fechaPublicacion,a_isbn)
       @m_autores = a_autores
       @m_titulo = a_titulo
       @m_num_edicion = a_num_edicion
       @m_editorial = a_editorial
       @m_fechaPublicacion = a_fechaPublicacion
       @m_isbn = a_isbn
    end
    #Devuelve la referencia formateada
    def to_s
        "#{@m_autores} #{@m_titulo} #{@m_num_edicion} #{@m_serie} #{@m_editorial} #{@m_fechaPublicacion} #{@m_isbn}"
    end
    
end
=begin
libro = Bibliografia.new(["Dave Thomas"],"Programming Ruby 1.9 & 2.0: The Programatic Programers Guide","4 edition","Progmatic Bookshelf","July 7, 2013",["ISBN-13: 978-1937785499","ISBN-10: 1937785491"])
p libro.to_s
p libro.to_s==("[\"Dave Thomas\"] Programming Ruby 1.9 & 2.0: The Programatic Programers Guide 4 edition  Progmatic Bookshelf July 7, 2013 [\"ISBN-13: 978-1937785499\", \"ISBN-10: 1937785491\"]")
=end